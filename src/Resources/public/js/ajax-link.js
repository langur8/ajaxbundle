var ajaxLink = {
	handler: null,
	init: function (handler) {

		if (typeof handler === 'undefined') {
			this.handler = new AjaxHandler;
		} else {
			this.handler = handler;
		}

		$('body').on('click', 'a[data-ajax="on"]', function (event) {
			var obj = this;
			event.preventDefault();
			event.stopImmediatePropagation();

			ajaxLink.handler.start();
			return $.ajax ({
				type: 'post',
				url: obj.href })
					.done(function (data) {
						ajaxLink.success(data);
					})
					.fail(function (jqXHR, textStatus, errorThrown) {
						ajaxLink.fail(jqXHR, textStatus, errorThrown);
					});
		});
	},
	success: function (response) {
		var handlerResponse = [];
		if(typeof(response.el) !== 'undefined') {	
			handlerResponse['el'] = [];
			for(var i = 0; i < response.el.length; i++) {
				handlerResponse['el'].push({response: response.el[i]});
			}		
		} else {			
			handlerResponse = {response: response};
		}		
		
		ajaxLink.handler.success(handlerResponse);
		ajaxLink.handler.stop();
	},
	fail: function (jqXHR, textStatus, errorThrown) {

		var response = {};

		if (typeof jqXHR.responseJSON !== 'undefined') {
			response.response = jqXHR.responseJSON;
		} else {
			response.message = errorThrown;
		}

		if (typeof (jqXHR.status) !== 'undefined') {
			response.status = jqXHR.status;
		}

		this.handler.fail(response);
		this.handler.stop(true);

	},
};