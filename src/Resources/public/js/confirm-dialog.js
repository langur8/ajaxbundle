/**
 * Confirm dialog plugin
 *
 * @copyright  Copyright (c) 2012 Jan Červený
 * @license    BSD
 * @link       confirmdialog.redsoft.cz
 * @version    1.0
 */


$(document).ready(function () {
	$('body').on('click', '.btn-confirm', function (event) {
		confirmDialog.show(this, event);
		return false;
	});
});

var confirmDialog = {
	handler: null,
	spinnerEl: $('#delete-modal'),
	show: function (obj, event) {

		event.preventDefault();
		event.stopImmediatePropagation();
		$("<div id='dConfirm' class='modal fade'><div class='modal-dialog'><div id='dConfirmContent' class='modal-content'></div></div></div>").appendTo('body');
		$('#dConfirmContent').html("<div id='dConfirmHeader' class='modal-header'></div><div id='dConfirmBody' class='modal-body'></div><div id='dConfirmFooter' class='modal-footer'></div>");
		$('#dConfirmHeader').html("<a class='close' data-dismiss='modal'>×</a><h3 id='dConfirmTitle'></h3>");
		$('#dConfirmTitle').html($(obj).data('confirm-title'));
		$('#dConfirmBody').html($(obj).data('confirm-text'));
		$('#dConfirmFooter').html("<a id='dConfirmOk' class='btn " + $(obj).data('confirm-ok-class') + "' data-dismiss='modal'>Ano</a><a id='dConfirmCancel' class='btn " + $(obj).data('confirm-cancel-class') + "' data-dismiss='modal'>Ne</a>");
		if ($(obj).data('confirm-header-class')) {
			$('#dConfirmHeader').addClass($(obj).data('confirm-header-class'));
		}
		if ($(obj).data('confirm-ok-text')) {
			$('#dConfirmOk').html($(obj).data('confirm-ok-text'));
		}
		if ($(obj).data('confirm-cancel-text')) {
			$('#dConfirmCancel').html($(obj).data('confirm-cancel-text'));
		}
		$('#dConfirmOk').on('click', function () {
			var tagName = $(obj).prop("tagName");
			if (tagName == 'INPUT') {
				var form = $(obj).closest('form');
				form.submit();
			} else {
				if (typeof this.handler === 'undefined') {
					confirmDialog.handler = new AjaxHandler;
				}

				confirmDialog.handler.start();
				return $.ajax({
					type: 'post',
					url: obj.href})
						.done(function (response) {
							if (response.id > 0 && $(obj).hasClass('btn-delete') && $(obj).hasClass('btn-datatable')) {
								//console.log($(obj). parents('table') . DataTable());
								$(obj).parents('table').DataTable().row($(obj).parents('tr')).remove().draw();
							}
							confirmDialog.success(response);
						})
						.fail(function (jqXHR, textStatus, errorThrown) {
							ajaxLink.fail(jqXHR, textStatus, errorThrown);
						});

			}
		});
		$('#dConfirm').on('hidden', function () {
			$('#dConfirm').remove();
		});
		$('#dConfirm').modal('show');
	},
	success: function (response) {
		confirmDialog.handler.success({response: response});
		confirmDialog.handler.stop();

	},
	fail: function (jqXHR, textStatus, errorThrown) {

		var response = {};

		if (typeof jqXHR.responseJSON !== 'undefined') {
			response.response = jqXHR.responseJSON;
		} else {
			response.message = errorThrown;
		}

		if (typeof (jqXHR.status) !== 'undefined') {
			response.status = jqXHR.status;
		}

		this.handler.fail(response);
		this.handler.stop(true);

	},
};