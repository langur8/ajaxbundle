var AjaxHandler = function() {
	this.showProgressModal = true,
	this.hideProgressModal = true,
	this.progressModal = $('#save-modal'),
	this.infoModal = $('#info-modal'),
    this.showProgressIcon = false,
    this.hideProgressIcon = false,
    this.progressIcon = $('#ajax-loader'),
	this.redirect = '',
	this.srollPosition = 0,

    this.handleShowProgressModal = function() {
        this.progressModal.modal('show');
    },

    this.handleHideProgressModal = function() {
        this.progressModal.modal('hide');
    },

    this.handleShowProgressIcon = function() {
        this.progressIcon.removeClass('hidden');
        this.progressIcon.show();
    },

    this.handleHideProgressIcon = function() {
        this.progressIcon.hide();
    },

	this.start = function() {
		if(this.showProgressModal === true) {
			this.handleShowProgressModal();
		}
		if(this.showProgressIcon === true) {
			this.handleShowProgressIcon();
		}
	},

	this.stop = function(force) {
		if((this.hideProgressModal === true && this.redirect === '')  || (typeof(force) !== 'undefined' && force === true)) {
			this.handleHideProgressModal();
		}
		if((this.hideProgressIcon === true && this.redirect === '')  || (typeof(force) !== 'undefined' && force === true)) {
			this.handleHideProgressIcon();
		}
	},

	this.showInfo = function(message) {

		$('#info-modal-content').html(message);
		this.infoModal.modal('show');

	},

	this.success = function (data) {

		this.preSuccess(data);

		this.srollPosition = $(window).scrollTop();

		this.processResponse(data);

		$("html").scrollTop(this.srollPosition);

		if(this.redirect !== '') {
//			if(typeof(data.response.id) !== 'undefined') {
//				this.redirect = this.redirect.replace('--id--', data.response.id);		
//			}
			window.location.replace(this.redirect);

		}

		this.postSuccess(data);
	},
	this.preSuccess = function (data) {},
	this.postSuccess = function (data) {},

	this.fail = function (data) {
		this.preFail(data);
		this.processResponse(data);
		this.postFail(data);
	},
	this.preFail = function (data) {},
	this.postFail = function (data) {},

	this.processResponse = function(data) {

		var elements = [];
		elements = this.createArray(data, elements);

		//console.log(elements);
		//console.log(data);

		var messages = [];
		if (typeof(data.message) !== 'undefined'  && data.message !== '') {
			messages.push(data.message);
		}

		for(var i = 0; i < elements.length; i++) {
			var obj = elements[i];
			if(typeof(obj.response.html) !== 'undefined') {
				if(typeof(obj.response.elementId) !== 'undefined' && typeof(obj.element) === 'undefined') {
					obj.element = $('#' + obj.response.elementId);
				}
				if(typeof obj.element !== 'undefined') {
					if(typeof(obj.response.replace) !== 'undefined' && obj.response.replace === true) {
						obj.element.replaceWith(obj.response.html);
					} else {
						obj.element.html(obj.response.html);
					}
				}
			}

			if(typeof(obj.response.redirect) !== 'undefined') {
				this.redirect = obj.response.redirect;
			}

			if (typeof(obj.response.message) !== 'undefined' && obj.response.message.trim() !== '') {
				messages.push(obj.response.message.trim())
			}
		}

		if (messages.length > 0) {
			this.showInfo(messages.join('<br />'));
		} else if(typeof(data.status) !== 'undefined' && data.status === 404) {
			this.showInfo("Chyba běhu skriptu. Zkuste prosím obnovit stránku.");
		}

	},

	this.createArray = function(data, elements) {

		//console.log(data);

		if(typeof(data.response) !== 'undefined') {
			elements.push(data);
			if(typeof(data.response.el) !== 'undefined') {
				elements = elements.concat(data.response.el);
			}
		}

		if(typeof(data.el) !== 'undefined') {
			for(var i = 0; i < data.el.length; i++) {
				if(typeof(data.el[i].response) !== 'undefined') {
					if(typeof(data.el[i].response.el) !== 'undefined') {
						//console.log(data.el[i].response.el);
						elements = this.createArray(data.el[i].response, elements);
						delete data.el[i].response.el;
					}
					elements.push(data.el[i]);
				}
			}
		}

		return elements;
	}
};


//var AjaxResponse = function(jsonResponse, status) {
//    this.response = jsonResponse;
//	this.status = status;
//	this.elements = [];
//	this.messages = [];
//	
//};
//AjaxResponse.prototype = {
//    getElements: function() {
//		return this.elements;
//    }
//};
//
//var AjaxElement = function(selector, html, replaceContent = false) {
//    this.selector = selector;
//	this.html = html;
//	this.replaceContent = replaceContent;
//};
//AjaxElement.prototype = {
//    render: function() {
//
//    }
//};
