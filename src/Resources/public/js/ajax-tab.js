var ajaxTab = {
	loadRepeat: false,
	loadFirst: true,
	init: function(id) {
		$('body').on('click', id + ' a[data-toggle="tabajax"]', function (e) {
			e.preventDefault();
			ajaxTab.load($(this));
			return false;
		});
		
		if(ajaxTab.loadFirst === true) {
			tab = $(id + ' a[data-toggle="tabajax"]:first');
			this.load(tab);
		}
		
	},
	load: function (tab) {
			
		loadurl = tab.attr('href'),
		targ = tab.attr('data-target');

		ajaxTab.beforeLoad(tab);
		$.getJSON(loadurl, function(data) {
			//
			$(targ + ' .tabajax-content').fadeOut(500, function() {
				$(targ + ' .tabajax-content').hide();
				$(targ + ' .tabajax-content').html(data.html);
				$(targ + ' .tabajax-content').fadeIn(500, function(){
					ajaxTab.afterLoad(tab);
				});				
			});
						
		});

		tab.tab('show');
		if(this.loadRepeat === false) {
			tab.attr('data-toggle', 'tab')
		}
		return false;	
	},	
	afterLoad: function(tab) {
		
	},	
	beforeLoad: function(tab) {
		
	}	

};