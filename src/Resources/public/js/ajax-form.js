var ajaxForm = {
	forms: [],
	queueEl: $('body'),
	handler: null,
	handlerContent: [],
	init: function (handler) {

		if (typeof handler === 'undefined') {
			this.handler = new AjaxHandler;
		} else {
			this.handler = handler;
		}

		$('body').on('submit', '.ajax-form', function (e) {
			e.preventDefault();
			ajaxForm.forms = [];
			ajaxForm.action(this);
			return false;
		});
	},
	action: function (form) {
		this.forms.push(form);
		this.queue();
	},
	submit: function (form) {

		var formObj = form;

		return $.ajax({
			type: $(form).attr('method'),
			url: this.getFormAction($(form)),
			data: new FormData(form),
			processData: false,
			contentType: false,
		})
				.done(function (data) {
					ajaxForm.success(data, formObj);
					ajaxForm.queueEl.dequeue();
				})
				.fail(function (jqXHR, textStatus, errorThrown) {
					ajaxForm.forms = [];
					ajaxForm.queueEl.clearQueue();
					ajaxForm.fail(jqXHR, textStatus, errorThrown, formObj);
				});
	},
	getFormAction: function (form) {
		return form.attr('action');
	},
	success: function (response, formObj) {
		this.processResponse(response, formObj);
	},
	fail: function (jqXHR, textStatus, errorThrown, formObj) {

		if (typeof jqXHR.responseJSON !== 'undefined') {
			this.processResponse(jqXHR.responseJSON, formObj);
		} else {
			this.handlerContent.message = errorThrown;
		}

		if (typeof (jqXHR.status) !== 'undefined') {
			this.handlerContent.status = jqXHR.status;
		}

		this.handler.fail({el: ajaxForm.handlerContent});
		this.handler.stop(true);

	},
	queue: function () {
		this.handler.start();
		this.handlerContent = [];
		$.each(this.forms, function (i, form) {
			ajaxForm.queueEl.queue(function () {
				ajaxForm.submit(form);
			})
		});

		this.queueEl.queue(function () {
			ajaxForm.queueEl.clearQueue();
			ajaxForm.handler.success({el: ajaxForm.handlerContent});
			ajaxForm.handler.stop();
		});
	},
	autofocus: function () {
		$('body').on('keyup', '.form-control', function () {
			if (typeof this.maxLength != "undefined" && $(this).val().length == this.maxLength) {

				//test current form group
				var input = $(this).parent().next().find('.form-control:first');
				if (input.length !== 0) {
					input.focus();
					return false;
				}

				//test next form group
				var input = $(this).closest('.form-group').nextAll('div.form-group:first').find('.form-control:first');
				if (input.length !== 0) {
					input.focus();
					return false;
				}

			}
		});
	},
	processResponse: function(response, formObj) {
		if (typeof (response.el) !== 'undefined') {
			for (var i = 0; i < response.el.length; i++) {
				ajaxForm.handlerContent.push({response: response.el[i]});
			}
 		} else if(typeof (response.elementId) !== 'undefined') {
			ajaxForm.handlerContent.push({
				element: $(response.elementId),
				response: response
			});		
		} else {
			ajaxForm.handlerContent.push({
				element: $(formObj).children('.form-content'),
				response: response
			});
		}
	}

};